/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.7.21 : Database - connectors
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`connectors` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `connectors`;

/*Table structure for table `connectionsource` */

DROP TABLE IF EXISTS `connectionsource`;

CREATE TABLE `connectionsource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `urls` varchar(100) DEFAULT NULL,
  `drivers` varchar(100) DEFAULT NULL,
  `datecreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `connectionsource` */

insert  into `connectionsource`(`id`,`username`,`password`,`port`,`urls`,`drivers`,`datecreated`) values (1,'root','timo',3306,'jdbc:mysql://localhost:3306/connectors','com.mysql.jdbc.Driver','2018-10-10 00:00:00'),(2,'taiye','taiye',3306,'jdbc:mysql://localhost:3306/taiye','com.mysql.jdbc.Driver','2018-10-10 00:00:00'),(3,'jidex','jidex',3306,'jdbc:mysql://localhost:3306/jidex','com.mysql.jdbc.Driver','2018-10-10 00:00:00'),(4,'funmi','funmi',3306,'jdbc:mysql://localhost:3306/funmi','com.mysql.jdbc.Driver','2018-10-10 00:00:00'),(5,'computer1','computer1',3306,'jdbc:mysql://localhost:3306/computer1','com.mysql.jdbc.Driver','2018-10-10 00:00:00'),(6,'computer2','computer2',3306,'jdbc:mysql://localhost:3306/computer2','com.mysql.jdbc.Driver','2018-10-10 00:00:00');

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `office` varchar(120) DEFAULT NULL,
  `datecreated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `employee` */

insert  into `employee`(`id`,`firstname`,`lastname`,`department`,`office`,`datecreated`) values (1,'Paul','Adeyemi','Engineering','Business','2018-10-20 00:00:00');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `users` int(11) DEFAULT NULL,
  `datecreated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_roles` (`users`),
  CONSTRAINT `FK_roles` FOREIGN KEY (`users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`users`,`datecreated`) values (1,'ADMIN',NULL,'2018-10-04 00:00:00'),(2,'DBM',NULL,'2018-10-02 00:00:00'),(3,'CLIENT',NULL,'2018-02-10 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `tenant` varchar(100) DEFAULT NULL,
  `roles` int(11) DEFAULT NULL,
  `datecreated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users` (`roles`),
  CONSTRAINT `FK_users` FOREIGN KEY (`roles`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
