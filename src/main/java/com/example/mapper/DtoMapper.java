/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.mapper;

import com.example.dto.Employeedto;
import com.example.model.Employee;

/**
 *
 * @author JIDEX
 */
public class DtoMapper {
    public static Employeedto maptoEmployeedto(Employee mapfrom){
        if(mapfrom==null){
            return null;
        }
        Employeedto mapto = new Employeedto();
       mapto.setDatecreated(mapfrom.getDatecreated());
        mapto.setDepartment(mapfrom.getDepartment());
        mapto.setFirstname(mapfrom.getFirstname());
        mapto.setId(mapfrom.getId());
        mapto.setLastname(mapfrom.getLastname());
        mapto.setOffice(mapfrom.getOffice());
        return mapto;
    }
    public static Employee maptoEmployee(Employeedto mapfrom){
        if(mapfrom==null){
            return null;
        }
        Employee mapto = new Employee();
        mapto.setDatecreated(mapfrom.getDatecreated());
        mapto.setDepartment(mapfrom.getDepartment());
        mapto.setFirstname(mapfrom.getFirstname());
        mapto.setId(mapfrom.getId());
        mapto.setLastname(mapfrom.getLastname());
        mapto.setOffice(mapfrom.getOffice());
        return mapto;
    }
}
